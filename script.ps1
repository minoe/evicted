param (
  [Parameter(Mandatory=$true)][string]$resourceGroup,
  [Parameter(Mandatory=$true)][string]$clusterName
)

$ErrorActionPreference = 'Stop'
# set path to kube config file
$kubePath = "{0}/.kube" -f $env:HOME
$kubeConfig = "{0}/config" -f $kubePath
# empty out kube config file

mkdir -p $kubePath
Set-Content -Path $kubeConfig -Value ''
chmod 0600 $kubeConfig

# get creds via az cli
az aks get-credentials -g $resourceGroup -n $clusterName -f $kubeConfig

# set count to zero for tracking number of evicted pods
$count = 0

# get list of pods
$evictedList = kubectl get pods --namespace default -o json | ConvertFrom-Json

# for each pod in list, check for evicted status.
foreach($item in $evictedList.items) {
  $podId = $item.metadata.name | Where-Object {$item.status.reason -eq 'Evicted'}
  # if pod has evicted status, delete it
  if($podId){
    kubectl delete pod $podId
    # increase counter to track number of evicted pods
    $count++
  }
}

# return number of evicted pods for the pipeline output
$message = '{0} pods evicted' -f $count
$slackURL = 'https://hooks.slack.com/services/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'

if($count -gt 0){
  $body = @{
    text = "$count pods evicted from $clusterName in $resourceGroup"
  }
  $jsonBody = $body | ConvertTo-Json
  Invoke-RestMethod -Method POST -Uri $slackURL -Body $jsonBody -ContentType 'application/json'
}

return $message
